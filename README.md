# Javascript
Intern: Nguyen Ngo Lap

## 1. Javascript: An introduction
A scripting language that allows writing small programs running on browsers and changing the HTML and CSS of the current document.

- Conforms to ECMAScript standard
- Browsers use ECMAScript to interpret JS

### Current Landscape(Spring 2017)

- ECMAScript 5.1 (2011): current standard (outdated)
- ECMAScript 2015 (ES6): emerging standard; needs a transpiler like Babel to work
- ECMAScript 2016 (ES7): in development

### jQuery

- Libraby of JS functions (write less, do more)
- CSS-like syntax, visual and UI enhancements

### JS Frameworks

- AngularJS, React, Vue.js, ...
- Front-end app frameworks used to simplify the building of advanced interactive web-based app

### Node.js

- JS platform built on browser runtime environments
- Used to run advanced operations and apps on servers and computers

## 2. The basics
###JS loading methods

- Immediate: Causes JS render blocking
- Asynchronous: Add `async` attribute. Render blocking only happens when the script is executed
- Deferred: Add `defer` attribue. Only executes the script after everything's been loaded

### How to write good JS

- JS is case sensitive
- Use camelCase
- Whitespace matters
- End each statement with a semicolon
- Use comments liberally

## 3. Working with data
\- Should use `var` prefix to create variables to avoid global declaration

\- Data types:

- Numeric
- String
- Boolean
- null: the intentional absence of a value
- undefined: placeholder when a variable is not set
- Symbol

\- The difference in order of `++/--`:

- Prefix: Do the operation with current value, then do the increment/decrement
- Suffix: Do the increment/decrement first, then do the operation with the new value

\- Comparing with `==/===`:

- `==`: More lenient (Like number compare with string of number)
- `===`: Absolute strict. Identical comparation (same data type)

\- Array reference: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

## 4. Functions and objects
\- 3 types of functions:

- Named functions
- Anonymous functions
- Immediately invoked function expressions

### Const and Let
\- `const`:

- Constant
- Can't be changed once defined

\- `let`:

- Block scope variable
- Smaller scope than `var`

### Closures
A function inside a function, but relies on variables from the outside function to work.

Reference: https://developer.mozilla.org/en/docs/Web/JavaScript/Closures

## 5. JS and the DOM, part 1: Changing DOM elements
\- The document is an object, and elements are nodes inside that object (properties) -> Elements can be retrieved with dot notation.

- E.g.: `document.body, document.title, document.URL. ...`

\- Get elements inside the body:

- Old methods:
  - `document.getElementById("some-ID")`
  - `document,getElementsByClassName("class-name")`
  - `document.geElementsByTagName("HTML tag")`
- New methods:
  - `document.querySelector(".main-nav a")` - Get the first element matching specified selector
  - `document.querySelectorAll(".post-content p")` - Get all elements matching specified selector

\- Access and change elements: `innerHTML`, `outer HTML`

\- Access and change classes:

- `.classlist.add/remove/item/toggle/contain/replace`
- Reference: https://developer.mozilla.org/en/docs/Web/API/Element/classList

\- Access and change attributes: `.hasAttribute`, `.getAttribute`, `.setAttribute`, `removeAttribute`

\- Adding new DOM elements:

1. Create the elements
2. Create the text node that goes inside the element
3. Add the text node to element
4. Add the element to the DOM tree

### Inline CSS

- `...querySelector("selector").style.[CSS property]`
- On the other hand, `style` is also an attribute -> The above methods for attribute can be used

\- Inline CSS in opinionated

- Inline CSS overrides whatever CSS is applied to an element
- In most cases, best practice is to create CSS rules and use JS to manage these classes to apply the rules to the element

## 7. JS and the DOM, part 2: Events
\- Typical events:

- Browser-level events: Browser behavior:
    - Load
    - Error
    - Online/Offline
    - Resize
    - Scroll
- DOM-level events: Content interaction:
    - Focus: clicked, tabbed, ...
    - Blur: lost focus
    - Resets/Submit: for forms
    - Mouse events: click, mouseover, drag, drop, ...
- Other events:
    - Media events: audio/video
    - Progress event: monitor ongoing progress
    - CSS transition event
- Reference:
    - https://developer.mozilla.org/en-US/docs/Web/Events
    - https://www.w3.org/TR/2013/WD-DOM-Level-3-Events-20131105/#event-flow

## 10. Automated Responsive Images Markup
\- Use `sizes` and `srcset`
\- Reference: https://ericportis.com/posts/2014/srcset-sizes/

## 11. Troubleshooting, validating and minifying JS
\- Console debugging methods: http://developer.mozilla.org/en/docs/Web/API/console

\- Script linting:

- Online:
    - JSLint
    - JSHint
- Automated:
    - ESLint

\- Minifying:

- Online:
    - http://www.minifier.org/
- Automated:
    - Uglify

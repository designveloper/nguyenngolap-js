const HOURHAND = document.querySelector("#hour");
const MINUTEHAND = document.querySelector("#minute");
const SECONDHAND = document.querySelector("#second");

let hrPos = 20;
let minPos = 30;
let secPos = 40;

// Degree per hour in a circle
const DEGPERHR = 360 / 12;
// Degree per minute in an hour
const DEGPERMIN = 360 / 60;
// Degree per second in a minute
const DEGPERSEC = 360 / 60;

var myClock = setInterval(function() {
  var date = new Date();

  let hr = date.getHours();
  let min = date.getMinutes();
  let sec = date.getSeconds();

  // 1. Set transition speed to 0s on returning to zero
  if (hr == 0 && min == 0 && sec == 0) {
    HOURHAND.setAttribute("style", "transform-origin: 300px 300px; transition: transform 0s;");
  } else {
    HOURHAND.setAttribute("style", "transform-origin: 300px 300px; transition: transform .5s ease-in-outs;");
  }

  if (min == 0 && sec == 0) {
    MINUTEHAND.setAttribute("style", "transform-origin: 300px 300px; transition: transform 0s;");
  } else {
    MINUTEHAND.setAttribute("style", "transform-origin: 300px 300px; transition: transform .5s ease-in-outs;");
  }

  if (sec == 0) {
    SECONDHAND.setAttribute("style", "transform-origin: 300px 300px; transition: transform 0s;");
  } else {
    SECONDHAND.setAttribute("style", "transform-origin: 300px 300px; transition: transform .5s ease-in-outs;");
  }

  // 2. Stacking second, minute and hour (avoid zero value)
  // 3. Disable transition completely

  hrPos = hr * DEGPERHR + (min / 60) * DEGPERHR;
  minPos = min * DEGPERMIN + (sec / 60) * DEGPERMIN;
  secPos = sec * DEGPERSEC;

  HOURHAND.style.transform = "rotate(" + hrPos + "deg)";
  MINUTEHAND.style.transform = "rotate(" + minPos + "deg)";
  SECONDHAND.style.transform = "rotate(" + secPos + "deg)";
}, 1000);
